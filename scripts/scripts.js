//Hamburger events
window.addEventListener('scroll', () => {
    if ($('.header__nav').hasClass('header__nav--show')) {
        $('.header__nav').addClass('header__nav--hide');
        $('.header__nav').removeClass('header__nav--show');
    }
});

$('body').on('click', (event) => {
    if($(event.target).hasClass('header__hamburger') ||
        $(event.target).hasClass('header__nav') ||
        $(event.target).hasClass('header__hamburger-icon')) {
            $('.header__nav').removeClass('header__nav--hide');
            $('.header__nav').addClass('header__nav--show');
    } else {
        if ($('.header__nav').hasClass('header__nav--show')) {
            $('.header__nav').addClass('header__nav--hide');
            $('.header__nav').removeClass('header__nav--show');
        }
    }
});


//Animated scroll
$('a[href*=\\#]').on('click', function () {
    let elementClicked = $(this).attr('href');
    $('html, body').stop().animate({
        scrollTop: $(elementClicked).offset().top
    },1000);
});

$('.btn_page-up').on('click', function () {
    $('html, body').stop().animate({
        scrollTop: 0
    },2000);
});


// Show and hide Button "Page up"
$(window).on('scroll', function () {
    if($(this).scrollTop() > $(this).height()) {
        $('.btn_page-up').show();
    } else {
        $('.btn_page-up').hide();
    }
});


// Hide warning block
$('.warning__close').on('click', function () {
    $('.warning').hide();
});


//Google map
let map;

function initMap() {
    // The location of 50.467615, 30.470486
    let work = {lat: 50.467615, lng: 30.470486};
    // The map, centered at Uluru
    let map = new google.maps.Map(
        document.getElementById('map'), {zoom: 16, center: work});
    // The marker, positioned at Uluru
    let marker = new google.maps.Marker({position: work, map: map});
}
